#!/usr/bin/python3/

# Taken from https://www.pyimagesearch.com/2019/05/13/object-detection-and-image-classification-with-google-coral-usb-accelerator/

from edgetpu.detection.engine import DetectionEngine
from imutils.video import VideoStream
from PIL import Image
import argparse, imutils, time, cv2

ap = argparse.ArgumentParser()
ap.add_argument("-m" , "--model" , required=True, help="path to TensorFlow Lite object detection model" )
ap.add_argument("-l" , "--labels" , required=True, help="path to labels file" )
ap.add_argument("-c" , "--confidence" , type=float, default=0.5, help="minimum probability to filter weak detections" )
args = vars(ap.parse_args())
labels = {}

for row in open(args["labels" ]):
	(classID, label) = row.strip().split(maxsplit=1)
	labels[int(classID)] = label.strip()

model = DetectionEngine(args["model"])
vs = VideoStream(src=0).start()
time.sleep(2.0)

while True:
	frame = vs.read()
	frame = imutils.resize(frame, width=500)
	orig = frame.copy()
	frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
	frame = Image.fromarray(frame)
	start = time.time()
	results = model.DetectWithImage(frame, threshold=args["confidence" ], keep_aspect_ratio=True, relative_coord=False)
	end = time.time()

	for r in results:
		box = r.bounding_box.flatten().astype("int" )
		(startX, startY, endX, endY) = box
		label = labels[r.label_id]
		cv2.rectangle(orig, (startX, startY), (endX, endY),
		(0, 255, 0), 2)
		y = startY - 15 if startY - 15 > 15 else startY + 15
		text = "{}: {:.2f}%" .format(label, r.score * 100)
		cv2.putText(orig, text, (startX, y), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)

	cv2.imshow("Frame" , orig)
	key = cv2.waitKey(1) & 0xFF
	if key == ord("q" ):
		break

cv2.destroyAllWindows()
vs.stop()
